<?php
//floaval:GET FLOAT VALUE OF A VARIABLE
$var='10.35THE';
$float_val=floatval($var);
echo $float_val;
echo "<br>";
/*
 OUTPUT:10.35
RETURN ONLY FLOAT VALUE
 */


#EMPTY:DETERMINE WHETHER A VARIABLE IS EMPTY
$var2=0;
if (empty($var2)) {
    echo "the variable is empty";
}
else
    echo $var2;

//OUTPUT:THE VARIABLE IS EMPTY
echo "<br>";
$var3=10;
if (empty($var3)) {
    echo "the varable is empty";
}
    else
    echo $var3;
echo "<br>";
//OUTPUT:10

#is_array:FINDS WHETHER A VARIABLE IS AN ARRAY
$var4=array(10,20,30);
echo is_array($var4)? 'ARRAY' : 'NOT ARRAY';
//OUTPUT:'ARRAY'

echo "<br>";

//IS_NULL:FINDS WHETHER A VARIABLE IS NULL
$var5=NULL;
echo is_null($var5);
//OUTPUT:TRUE(1)
echo "<br>";


//ISSET:DETERMINE IS A VARIABLE IS SET OR NOT
$var6=20;
if (isset($var6))
{
    echo "the variable is not null";
}
else
    echo "the variable is null";
echo "<br>";
//OUTPUT:THE VARIABLE IS NOT NULL

//PRINT_R:PRINT HUMANE READABLE INFORMATION ABOUT VARIABLE
$var7=array(10,20,'string');
print_r($var7);
//OUTPUT:ARRAY([0]=10,[1]=20,[3]='STRING')
echo "<br>";

//UNSET:UNSET A SPECIFIED VARIABLE
$var8="set";
unset($var8);
//echo $var8; THEN THE OUTPUT SHOW THAT THE VARIABLE IS UNDEFINED.
$var8="show";
echo $var8;
//OUTPUT:SHOW
echo "<br>";

#VAR_DUMP:DUMPS INFORMATION ABOOUT A VARIABLE ALONG WITH LENGH AND DATA TYPE
$var9=array(10,20,"string");
var_dump($var9);
//OUTPUT:ARRAY(3){[0]=INT(10),[1]=INT(20),STRING(STRING)}
echo "<br>";

//VAR_EXPORT:OUTPUT OR RETURN PARSABLE STRING REPRESNTATION OF A VARIABLE or compound string representation
$var10=array(10,20,array('a','b'));
var_export($var10);
echo "<br>";

//GETTYPE:GET THE TYPE OF A VARIABLE
$var11=array(10,20,new stdClass,null,'string');
echo gettype($var11);
//OUTPUT:ARRAY
echo "<br>";

//IS_BOOL:FIND OUT WHETHER A VARIABLE IS BOOLEAN
$var12=1;
if (is_bool($var12)==true)
{
    echo "the variable is boolien";
}
else
     echo "the variable is not boolien";
//OUTPUT:THE VARIABLE IS not BOOLIEN
echo "<br>";
//IS_FLOAT:FINDS WHETHER THE TYPE OF VARIABLE OR DATA IS FLOAT
$var13=10.20;
if (is_float($var13))
{
    echo "data type is float";
}
else
    echo "data type is not float";
//OUTPUT:DATA TYPE IS FLOAT
echo "<br>";

//IS_STRING:DETERMINE WHETHER THE TYPE OF VARIABLE IS STRING
$var14="string";
if (is_string($va14))
{
    echo "data type is string";
}
else
    echo "data type is not string";
//OUTPUT:DATA TYPE IS STRING
echo "<br>";

//IS_SCALAR:FINDS WHETHER THE VALUE IS SCALAR.SCALAR ARE:INT, FLOAT,BOOL,STRING
$var15="string";
if (is_scalar($var15))
{
    echo $var15 . " is scalar type";
}
else
    echo $var15 ."is not scalar";
?>